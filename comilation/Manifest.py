# Project description
description = "Compilation of the template_module"

# Compiling the design
action = "synthesis"

# Compilation tool is Quartus v17.1
syn_tool = "quartus"
syn_tool_version = "17.1"

# Targeted device
syn_family = "Cyclone V"
syn_device = "5CGXFC5C6F27C7"

# Top module used for compilation
top_module = "ADC2UART_top"

# List of modules
modules = {
	"local" : [
        "$PROJECT_ROOT_PATH/src/fpga",
        ],
}

# List of source files for compilation
files = [
    "ADC2UART_top.sdc",
    "ADC2UART_top.stp",
    "ADC2UART_pins.tcl",
]

# Project configuration
configuration = {
	'ADC2UART_ENABLE': {'description': 'Enable ADC2UART_module in the design', 'value': 'True'},
}
