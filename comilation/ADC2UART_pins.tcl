# Copyright (C) 2017  Intel Corporation. All rights reserved.
# Your use of Intel Corporation's design tools, logic functions 
# and other software and tools, and its AMPP partner logic 
# functions, and any output files from any of the foregoing 
# (including device programming or simulation files), and any 
# associated documentation or information are expressly subject 
# to the terms and conditions of the Intel Program License 
# Subscription Agreement, the Intel Quartus Prime License Agreement,
# the Intel FPGA IP License Agreement, or other applicable license
# agreement, including, without limitation, that your use is for
# the sole purpose of programming logic devices manufactured by
# Intel and sold by Intel or its authorized distributors.  Please
# refer to the applicable agreement for further details.

# Quartus Prime Version 17.1.0 Build 590 10/25/2017 SJ Standard Edition
# File: /home/srdejong/LASP/LASP/src/ADC2UART/comilation/ADC2UART_pins.tcl
# Generated on: Tue Jul 24 09:01:43 2018

package require ::quartus::project

set_location_assignment PIN_D11 -to ADC_DA[13]
set_location_assignment PIN_D12 -to ADC_DA[12]
set_location_assignment PIN_E10 -to ADC_DA[11]
set_location_assignment PIN_E11 -to ADC_DA[10]
set_location_assignment PIN_C9 -to ADC_DA[9]
set_location_assignment PIN_B9 -to ADC_DA[8]
set_location_assignment PIN_D10 -to ADC_DA[7]
set_location_assignment PIN_C10 -to ADC_DA[6]
set_location_assignment PIN_A12 -to ADC_DA[5]
set_location_assignment PIN_B11 -to ADC_DA[4]
set_location_assignment PIN_B10 -to ADC_DA[3]
set_location_assignment PIN_A11 -to ADC_DA[2]
set_location_assignment PIN_C20 -to ADC_DA[1]
set_location_assignment PIN_B19 -to ADC_DA[0]
set_location_assignment PIN_H14 -to ADC_DB[13]
set_location_assignment PIN_H13 -to ADC_DB[12]
set_location_assignment PIN_N12 -to ADC_DB[11]
set_location_assignment PIN_M12 -to ADC_DB[10]
set_location_assignment PIN_M11 -to ADC_DB[9]
set_location_assignment PIN_L11 -to ADC_DB[8]
set_location_assignment PIN_H18 -to ADC_DB[7]
set_location_assignment PIN_H17 -to ADC_DB[6]
set_location_assignment PIN_L12 -to ADC_DB[5]
set_location_assignment PIN_K11 -to ADC_DB[4]
set_location_assignment PIN_H15 -to ADC_DB[3]
set_location_assignment PIN_J16 -to ADC_DB[2]
set_location_assignment PIN_J12 -to ADC_DB[1]
set_location_assignment PIN_J11 -to ADC_DB[0]
set_location_assignment PIN_L9 -to UART_TX
set_location_assignment PIN_M9 -to UART_RX
set_location_assignment PIN_AC9 -to triggerSwitch
set_location_assignment PIN_AE10 -to triggerSlopeSwitch
set_location_assignment PIN_AD13 -to delaySwitch
set_location_assignment PIN_A18 -to FPGA_CLK_A_N
set_location_assignment PIN_A19 -to FPGA_CLK_A_P
set_location_assignment PIN_A16 -to FPGA_CLK_B_N
set_location_assignment PIN_A17 -to FPGA_CLK_B_P
set_location_assignment PIN_C15 -to ADA_OE
set_location_assignment PIN_G17 -to ADB_OE
set_location_assignment PIN_G12 -to ADB_SPI_CS
set_location_assignment PIN_B22 -to ADA_SPI_CS
set_location_assignment PIN_F12 -to AD_SCLK
set_location_assignment PIN_A21 -to AD_SDIO
set_location_assignment PIN_L8 -to ADA_DCO
set_location_assignment PIN_K9 -to ADB_DCO
set_location_assignment PIN_G7 -to led[3]
set_location_assignment PIN_G6 -to led[2]
set_location_assignment PIN_F6 -to led[1]
set_location_assignment PIN_F7 -to led[0]
set_location_assignment PIN_H12 -to clk_50
set_location_assignment PIN_Y18 -to hex0[6]
set_location_assignment PIN_Y19 -to hex0[5]
set_location_assignment PIN_Y20 -to hex0[4]
set_location_assignment PIN_W18 -to hex0[3]
set_location_assignment PIN_V17 -to hex0[2]
set_location_assignment PIN_V18 -to hex0[1]
set_location_assignment PIN_V19 -to hex0[0]
set_location_assignment PIN_AF24 -to hex1[6]
set_location_assignment PIN_AC19 -to hex1[5]
set_location_assignment PIN_AE25 -to hex1[4]
set_location_assignment PIN_AE26 -to hex1[3]
set_location_assignment PIN_AB19 -to hex1[2]
set_location_assignment PIN_AD26 -to hex1[1]
set_location_assignment PIN_AA18 -to hex1[0]
set_location_assignment PIN_W20 -to hex2[6]
set_location_assignment PIN_W21 -to hex2[5]
set_location_assignment PIN_V20 -to hex2[4]
set_location_assignment PIN_V22 -to hex2[3]
set_location_assignment PIN_U20 -to hex2[2]
set_location_assignment PIN_AD6 -to hex2[1]
set_location_assignment PIN_AD7 -to hex2[0]
set_location_assignment PIN_AC22 -to hex3[6]
set_location_assignment PIN_AC23 -to hex3[5]
set_location_assignment PIN_AC24 -to hex3[4]
set_location_assignment PIN_AA22 -to hex3[3]
set_location_assignment PIN_AA23 -to hex3[2]
set_location_assignment PIN_Y23 -to hex3[1]
set_location_assignment PIN_Y24 -to hex3[0]
