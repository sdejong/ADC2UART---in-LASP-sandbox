# Project description
description = "Simulation of template module"

# Simulating the design
action = "simulation"

# Simulation tool is Modelsim v10.2c
sim_tool = "modelsim"
#sim_tool_version = "10.2c"

# Compilation tool is Quartus v17.1, used in simulation for Altera primitives
syn_tool = "quartus"
syn_tool_version = "17.1"

# Targeted device
syn_family = "Cyclone V"
syn_device = "5CGXFC5C6F27C7"

# Top module used for simulation
top_module = "work.ADC2UART_tb"

# Waveforms for simulation
sim_do_cmd = "wave.do"

# List of modules
modules = {
    "local" : [
        "$REPOSITORY_ROOT_PATH/common/src/sim",
        "$PROJECT_ROOT_PATH/src/fpga",
        ],
}

# Default library
library = "work"

# List of source files for the Reset_module testbench
files = [
    "ADC2UART_tb.vhd",
]

# Project configuration override
configuration = {
	'ADC2UART_ENABLE': {'description': 'Enable ADC2UART_module for simulation', 'value': 'True'},
}
