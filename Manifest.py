# List of modules
modules = {
    "local" : [
		"$PROJECT_ROOT_PATH/src/ADC2UART/src",
                "$PROJECT_ROOT_PATH/src/ADC2UART/altera/17.1/pro",
                #"$PROJECT_ROOT_PATH/src/ADC2UART/altera",
	],
}

# Default library
library = "work"

# List of source files
files = [
	"ADC2UART_top.vhd",
]
