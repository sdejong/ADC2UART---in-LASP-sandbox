# List of modules
modules = {
    "local": [
    ],
}

# Default library
library = 'template_altera'

# List of source files
files = [
    "lpm_pll.qsys",
    "UART_pll.qsys",
    "FIR_ofc.qsys",
]
