# List of modules
modules = {
    "local" : [
        "$HDLMAKE_QUARTUS_VERSION_MAJOR.$HDLMAKE_QUARTUS_VERSION_MINOR",
    ],
}


# Default library
library = 'template_altera'

# List of source files for the altera module
files = [
]
