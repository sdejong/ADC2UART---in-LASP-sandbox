# Library
library = "work"

# List of source files for the module
files = [
    "acquireSwitch.vhd",
    "ADC_handler.vhd",
    "adcSync.vhd",
    "charReader.vhd",
    "delayArray.vhd",
    "delayVec.vhd",
    "DSP.vhd",
    "flipSwitch.vhd",
    "signalToUART.vhd",
    "signer.vhd",
    "tenCount.vhd",
    "UART_handler.vhd",
    "trigger.vhd",
    "unsigner.vhd",
    "waveformGenerator.vhd",
    "ADC_Mux.vhd",
    "bin2dec.vhd",
    ]
